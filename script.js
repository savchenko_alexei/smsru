window.youplatformSMSRU = {};
window.youplatformSMSRU.render = [];
window.youplatformSMSRU.init = [];
window.youplatformSMSRU.initSettings = [];
window.youplatformSMSRU.initReport = [];
window.youplatformSMSRU.initSignatures = [];
window.youplatformSMSRU.initReg = [];
window.youplatformSMSRU.bind_actions = [];
window.youplatformSMSRU.settings = [];
window.youplatformSMSRU.onSave = [];
window.youplatformSMSRU.dpSettings = [];
window.youplatformSMSRU.destroy = [];
window.youplatformSMSRU.contacts = [];
window.youplatformSMSRU.leads = [];
window.youplatformSMSRU.tasks = [];
window.youplatformSMSRU.execute = function(event, widget){
    var result = true;
    for(var i = 0; i < window.youplatformSMSRU[event].length; i++){
        if(result){
            result = result && window.youplatformSMSRU[event][i](widget);
        }
    }
    return result;
};

define(['jquery',
    'lib/components/base/modal',
    './plugins/select2/select2.min.js',
    'https://youmelon.com/smsru/js/functions.js?' + Date.now(), // удалить
    'https://youmelon.com/smsru/js/settings.js?' + Date.now(),
    'https://youmelon.com/smsru/js/bind_actions.js?' + Date.now(),
    'https://youmelon.com/smsru/js/dpSettings.js?' + Date.now(),
    'https://youmelon.com/smsru/js/render.js?' + Date.now(),
    'https://youmelon.com/smsru/js/onSave.js?' + Date.now(),
    'https://youmelon.com/smsru/js/contacts.js?' + Date.now(),
    'https://youmelon.com/smsru/js/leads.js?' + Date.now(),
    'https://code.jquery.com/ui/1.12.1/jquery-ui.js'
], function($, Modal){

    // enter here script whith functions.js

    return CustomWidget;
});
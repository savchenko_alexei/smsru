window.youplatformSMSRU.bind_actions.push(
    function(self) {
        var area = AMOCRM.getWidgetsArea(),
			lang = self.i18n('userLang'),
			settings = self.get_settings(), 
			temp_tpl = settings.templates, 
			tpid = 0,
			twig = require('twigjs');

		$(document).off('click', '.select2-selection__choice__remove').on('click', '.select2-selection__choice__remove', function (e) {
            e.stopPropagation();
            e.preventDefault();
        });	
			
		$(document).on('change' + self.ns, '#smsru_date', function () {
			var date = $(this).val(),
			  time = $('#smsru_time').val();
			result = lang.sending + ': ' + date + ' ' + time;
			if (date !== '') {
			  $('#smsru_sending_input').val(result);
			}
		});

		$(document).on('blur' + self.ns, '#smsru_time', function () {
			var date = $('#smsru_date').val(),
				time = $(this).val();
			result = lang.sending + ': ' + date + ' ' + time;
			if (date !== '' && time !== '') {
				$('#smsru_sending_input').val(result);
			}
		});
		
		$(document).off('click', '.smsru_sending_field').on('click', '.smsru_sending_field', function (e) {
			var $popup = $('.smsru_sending_popup');
            if ($popup.css('display') != 'block') {
				$popup.show();
				var firstClick = true;
				$(document).bind('click.closeEvent', function (e) {
					if (!firstClick && $(e.target).closest($popup).length == 0) {
						$popup.hide();
						$(document).unbind('click.closeEvent');
					}
					firstClick = false;
				});
            }
            e.preventDefault();
		});
		
		$(document).off('click', '.smsru_edit_templates').on('click', '.smsru_edit_templates', function (e) {
            
			var lang = self.i18n('userLang'), settings = self.get_settings();
            self.set_macros_names();
			self.templates_editor(settings, lang);
			
            $('input#smsru_templates').attr('value', -2);
            $('input#smsru_templates').trigger('controls:change');
            $('input#smsru_templates').trigger('controls:change:visual');
        });
		
		/*
		$(document).on('click' + self.ns, '#smsru_tp_macroses .marker-list__tag', function () {
			self.insertTextAtCursor(document.getElementById('smsru_tpcontent'), $(this).text());
			$('#smsru_tpcontent').focus();
		});
		*/
		
		$(document).on('click' + self.ns, '#smsru_sendbtn', function () {
            var phones = $('#smsru-multiple-select').val();
			self.send_sms(phones, settings, lang);
		});
		
		$(document).on('change' + self.ns, '#smsru_templates', function () {
			var selector = self.system().area == 'digital_pipeline' ? $('#smsru_dp_message') : $('#smsru_smsmes');
				//sender_num = $('div#smscru_wrap_tpl li.control--select--list--item-selected').data('value');

			//if (sender_num == -1 && $('div.smscru-modal-window'))
			if ($('div.smscru-modal-window'))
				$('button.js-trigger-save').click();

			//self.get_sms_text(sender_num, selector);
		});
        
        return true;
    }
);
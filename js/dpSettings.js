window.youplatformSMSRU.dpSettings.push(
    function(self){
        var area = AMOCRM.getWidgetsArea();
        var w_code = self.get_settings().widget_code;
        var twig = require('twigjs');
		var settings = self.get_settings();
        var lang = self.i18n('settings');
        var userLang = self.i18n('userLang');
		
		self.set_macros_names();
		
        //$('.digital-pipeline__edit-bubble').css({'width': '500px'});

		var api_key_dp = $(".digital-pipeline__short-task_widget-style_" + w_code).parent().parent().find("input[name=api_key_dp]");
		var api_key_dp_label = api_key_dp.parent().parent();
		api_key_dp.val(self.get_settings().api_key);
		api_key_dp_label.find('[title =' + lang.api_key_dp + ']').hide();
		api_key_dp.attr({type: 'hidden', class: ''});
		
		var message = $(".digital-pipeline__short-task_widget-style_" + w_code).parent().parent().find("input[name=message]");
		var message_label = message.parent().parent();
		var value_message = message.val();
		//message_label.find('[title =' + lang.message + ']').hide();
		//message.attr({type: 'hidden', class: ''});
		
		var sender = $(".digital-pipeline__short-task_widget-style_" + w_code).parent().parent().find("input[name=sender]");
		var label_sender = sender.parent().parent();
		var value_sender = sender.val();
		//label_sender.find('[title =' + lang.from + ']').hide();
		//sender.attr({type: 'hidden', class: ''});
		
		var recipients = $(".digital-pipeline__short-task_widget-style_" + w_code).parent().parent().find("input[name=recipients]");
		var label_recipients = recipients.parent().parent();
		var value_recipient = recipients.val();
		if(!value_recipient){
			recipients.val(1);
		}
		//label_recipients.find('[title =' + lang.recipients + ']').hide();
		//recipients.attr({type: 'hidden', class: ''});
		
		var recipient = $(".digital-pipeline__short-task_widget-style_" + w_code).parent().parent().find("input[name=recipient]");
		var label_recipient = recipient.parent().parent();
		var value_recipient = recipient.val();
		//label_recipient.find('[title =' + lang.recipient + ']').hide();
		//recipient.attr({type: 'hidden', class: ''});
		
		$('#widget_settings__fields_wrapper').css('display', 'none');
		
		var smsru_templates = [
			{
				id: 0,
				option: 'Выберите шаблон'
			}
		];
		
		smsru_templates.push({
            option: 'Редактор шаблонов',
            class_name: 'smsru_edit_templates',
            id: -1
        });
		
		var smsru_senders = [
			{
				id: 0,
				option: 'Выберите отправителя'
			}
		];
		
		var smsru_recipients = [
			{
				id: 0,
				option: 'Выберите получателя'
			}
		];
		
		var macros_block = '<div id="smsru_tp_macroses" class="custom-scroll">';
		var macros_tpl = '<div class="sms_ru_tpl_markers">' +
			'<div class="sms_ru_tpl_markers__title">' +
			'{{ macros_title }}' +
			'</div>' +
			'{% for item in macroses %}' +
			'<p class="marker-list__row"><span class="marker-list__tag-bot" data-marker="{{ item.macros }}">' +
			'<span class="marker-list__tag">{{ item.macros }}</span></span>' +
			'<span class="marker-list__tag-descr"> - {{ item.title }} </span></p>' +
			'{% endfor %}' +
		'</div>';

		if (_.contains(['leads', 'leads-dp'], AMOCRM.data.current_entity)) {
          macros_block += self.render({data: macros_tpl}, {macroses: settings.leads_macros, macros_title: userLang.leadsMacroses})
        }
        if (_.contains(['customers', 'customers-dp'], AMOCRM.data.current_entity)) {
          macros_block += self.render({data: macros_tpl}, {macroses: settings.customers_macros, macros_title: userLang.customersMacroses})
        }
        if (_.contains(['leads', 'leads-dp', 'customers', 'customers-dp', 'contacts'], AMOCRM.data.current_entity)) {
          macros_block += self.render({data: macros_tpl}, {macroses: settings.contacts_macros, macros_title: userLang.contactsMacroses})
        }
        macros_block += self.render({data: macros_tpl}, {macroses: settings.companies_macros, macros_title: userLang.companyMacroses})

		macros_block += '</div>';
		
		var dp_content_block = '<div class="smsru-form-dp">' +
			
			'<div class="smsru-form-margin">' + self.render_select('smsru_templates', smsru_templates, '', '', 'smsru_templates') + '</div>' +
			
			'<div class="smsru-form-margin">' + self.render_textarea('smsru_textarea', userLang.textSMS + '&hellip;', 'smsru-textarea-dp', '') + '</div>' +
			
			'<div class="smsru-form-margin">' + self.render_select('smsru_senders', smsru_senders, '', '', '') + '</div>' +
			
			'<div class="smsru-form-margin">' + self.render_select('smsru_recipients', smsru_recipients, '', '', '') + '</div>' +
			
			'<div class="smsru-form-margin">' + self.render_checkbox('smsru_recipient', 'Применить только к основному контакту', false, '', 'smsru_recipient') + '</div>' +
			
			'<div class="smsru-form-margin">' + macros_block + '</div>' +
			
		'</div>';

		$('#widget_settings__fields_wrapper').after(dp_content_block);

        return true;
    }
);
var CustomWidget = function () {
	var self = this,
		system = self.system,
		version = '0.0.1',
		area = AMOCRM.getWidgetsArea(),
		twig = require('twigjs'),
		cd = new Date;
		
	var Modal = require('lib/components/base/modal');	

	this.getServerUrl = function(){
		var serverUrl = 'https://youmelon.com/smsru';
		return serverUrl;
	};
	
	this.date_field = function(id, class_name) {
		return twig({
			ref: '/tmpl/controls/date_field.twig'
		}).render({
			id: id,
			class_name: class_name
		});
	};
	
	this.render_suggest = function(items, id, class_name) {
		return twig({
			ref: '/tmpl/controls/suggest.twig'
		}).render({
			items: items,
			id: id,
			class_name: class_name
		});
	};
	
	this.render_select = function(name, items, selected, class_name, id) {
		return twig({
			ref: '/tmpl/controls/select.twig'
		}).render({
			name: name,
			items: items,
			selected: selected,
			class_name: class_name,
			id: id,
		});
	};

	this.render_input = function(name, placeholder, value, class_name, id, readonly) {
		return twig({
			ref: '/tmpl/controls/input.twig'
		}).render({
			name: name,
			value: value,
			placeholder: placeholder,
			class_name: class_name,
			id: id,
			readonly: readonly
		});
	};
	
	this.render_textarea = function(name, placeholder, class_name, id) {
		return twig({
			ref: '/tmpl/controls/textarea.twig'
		}).render({
			name: name,
			id: id,
			placeholder: placeholder,
			class_name: class_name
		});
	};
	
	this.render_checkbox = function(name, text, checked, class_name, id) {
		return twig({
			ref: '/tmpl/controls/checkbox.twig'
		}).render({
			name: name,
			id: id,
			text: text,
			checked: checked,
			class_name: class_name
		});
	};

	this.render_button = function(name, text, class_name, id) {
		return twig({
			ref: '/tmpl/controls/button.twig'
		}).render({
			id: id,
			name: name,
			text: text,
			class_name: class_name
		});
	};
	
	this.getTemplate = function (template, callback) {
		template = template || '';

		return self.render({
			href: '/tmpl/' + template + '.twig',
			base_path: self.params.path,
			v: version,
			load: callback
		}, {});
	};
	
	this.addZero = function (i) {
        if (i < 10) {
          i = '0' + i;
        }
        return i;
	};
	
	this.selectAllContacts = function () {
		var selectedItems = [];

		var allOptions = $("#smsru-multiple-select option");

		allOptions.each(function () {
			selectedItems.push($(this).val());
		});
		return selectedItems;
	};
	
	this.getContactList = function () {
		var area = self.system().area;
		var $selector, contacts_collection = [], selected_phones = [];
		if ($.inArray(area, ['ccard', 'comcard', 'lcard', 'cucard']) != -1) {
			$selector = $('[data-pei-code=phone] input[type=text]');
			$selector.each(function () {
				if ($(this).val().trim().match(/^\+?(?:[- ()]*\d[- ()]*){10,15}$/) &&
					$.inArray($(this).val().replace(/[^0-9]/ig, ""), selected_phones) === -1) {
					selected_phones.push($(this).val().replace(/[^0-9]/ig, ""));
					contacts_collection.push({
						id: $(this).val(),
						text: $(this).val()
					});
				}
			});
		}

		return contacts_collection;
	};
	
	this.set_macros_names = function () {
        $.ajax({
			type: 'GET',
			url: '/private/api/v2/json/accounts/current',
			dataType: 'json',
			async: false,
			success: function (data) {
				var macroses = [], field, cf = data.response.account.custom_fields;

				for (field in cf) {
					switch (field) {
						case 'companies':
							macroses[0] = [{
								macros: '{{company_name}}',
								title: self.i18n('userLang').macrosTitle
							}];
							macroses[0] = macroses[0].concat(self.create_macros_names(field, cf[field]));
							break;
						case 'contacts':
							macroses[1] = [{
								macros: '{{contact_name}}',
								title: self.i18n('userLang').macrosTitle
							}];
							macroses[1] = macroses[1].concat(self.create_macros_names(field, cf[field]));
							break;
						case 'leads':
							macroses[2] = [
								{
									macros: '{{lead_name}}',
									title: self.i18n('userLang').macrosTitle
								},
								{
									macros: '{{leads.price}}',
									title: self.i18n('userLang').macros_leads_price
								}
							];
							macroses[2] = macroses[2].concat(self.create_macros_names(field, cf[field]));
							break;
						case 'customers':
							macroses[3] = [
								{
									macros: '{{customer_name}}',
									title: self.i18n('userLang').macrosTitle
								},
								{
									macros: '{{customers.next_price}}',
									title: self.i18n('userLang').macros_customers_next_price
								},
								{
									macros: '{{customers.next_date}}',
									title: self.i18n('userLang').macros_customers_next_date
								}
							];
							macroses[3] = macroses[3].concat(self.create_macros_names(field, cf[field]));
							break;
					}
				}

				self.set_settings({
					companies_macros: macroses[0],
					contacts_macros: macroses[1],
					leads_macros: macroses[2],
					customers_macros: macroses[3],
					account_users: data.response.account.users
				});
			}
        });
	};
	
	 this.insertTextAtCursor = function (el, text, offset) {
        var val = el.value, endIndex, range, doc = el.ownerDocument;
		if (typeof el.selectionStart == "number"
		&& typeof el.selectionEnd == "number") {
			endIndex = el.selectionEnd;
			el.value = val.slice(0, endIndex) + text + val.slice(endIndex);
			el.selectionStart = el.selectionEnd = endIndex + text.length + (offset ? offset : 0);
		} else if (doc.selection != "undefined" && doc.selection.createRange) {
			el.focus();
			range = doc.selection.createRange();
			range.collapse(false);
			range.text = text;
			range.select();
		}
	};
	
	this.get_customers_phones = function (sel_customers) {
		var query = '', lang = self.i18n('userLang'), links, result;

		_.each(sel_customers, function (item, index) {
			query += 'links['+index+'][from]=customers&links['+index+'][to]=contacts&links['+index+'][from_id]='+item.id + '&';

		});

		$.ajax({
			type: 'GET',
			url: '/private/api/v2/json/links/list',
			data: query,
			dataType: 'json',
			async: false,
			success: function (data) {
				var query = '', phones;
				if (Fn.hasKeys(data, ['response', 'links'])) {
					links = data.response.links;
					_.each(links, function (link) {
						query +='id[]=' + link.to_id + '&';
					})
				}
				if (query !== '') {
					data = self.get_elements_info(query, 'contacts');
				}
				if (Fn.hasKeys(data, ['response', 'contacts'])) {
					result = data.response.contacts;
					//$('#prostor_sendbtn').removeClass('hidden');
				} else {
					//$('#prostor_err').text(lang.getContactInfoError);
					//$('#prostor_sendbtn').addClass('hidden');
				}
				_.each(result, function (contact) {
					var linked = _.map(links, function (link) {
						if (contact.id === link.to_id) {
							return link.from_id;
						}
					});
					if (Fn.hasKeys(contact, ['linked_customers_id'])) {
						contact.linked_customers_id.push(linked);
					} else  {
						contact.linked_customers_id = linked;
					}
				});

				phones = Fn.getPhones(result);

				$('.multiple-select').select2({
					data: phones,
					tags: true
				});

				$(".multiple-select").val(self.selectAllContacts()).trigger("change");

				$('.select2-selection__rendered').addClass('custom-scroll');
				$('.multiple-select').on('select2:open', function () {
					$('.select2-selection__rendered, .select2-results__options').addClass('custom-scroll');
				});
			}
		});

		return result;
	};

	this.get_leads_phones = function (sel_leads) {
		var query = '', lang = self.i18n('userLang'), contacts_data = [];

		for (i = 0; i < sel_leads.length; i++)
			query += 'deals_link[]=' + sel_leads[i].id + '&';

		$.ajax({
			type: 'GET',
			url: '/private/api/v2/json/contacts/links',
			data: query,
			dataType: 'json',
			async: false,
			success: function (data) {
				var links = [],query = '', phones;

				if (Fn.hasKeys(data, ['response', 'links'])) {
					links = data.response.links;
				}
				_.each(links, function (link) {
					query += 'id[]=' + link.contact_id + '&';
				});

				if (query !== '') {
					contacts_data = self.get_elements_info(query, 'contacts');
				}
				if (!Fn.hasKeys(contacts_data, ['response', 'contacts'])) {
					self.openModalMessage(lang.getContactInfoError);
				} else {
					contacts_data = contacts_data.response.contacts;
				}

				phones = Fn.getPhones(contacts_data);

				$('.multiple-select').select2({
					data: phones,
					tags: true
				});

				$(".multiple-select").val(self.selectAllContacts()).trigger("change");

				$('.select2-selection__rendered').addClass('custom-scroll');
				$('.multiple-select').on('select2:open', function () {
					$('.select2-selection__rendered, .select2-results__options').addClass('custom-scroll');
				});
			}
		});

		return contacts_data;
	};

	this.get_raw_list = function (data, type) {
		var raw_data,
			list = [],
			deals_ct = [],
			customers_ct =[],
			phl = [],
			//phl = self.selectAllContacts(),
			unlinked_phones;
		
		var phone_elements = self.selectAllContacts();
		for(var i = 0; i < phone_elements.length; i++){
			phl.push(phone_elements[i].trim().replace(/[^0-9]/ig, ""));
		}

		if (type === 'contacts' || type === 'company') {
			raw_data = data.response.contacts;
		}
		else if (type === 'customers') {
			raw_data = data.response.customers;
			customers_ct = self.get_customers_phones(raw_data);
		}
		else {
			raw_data = data.response.leads;
			deals_ct = self.get_leads_phones(raw_data);
		}

		phones = [];
		_.each(raw_data, function (entity) {
			var numeric_type, custom_field;

			switch (type) {
				case 'contacts':
					numeric_type = 1;
					break;
				case 'leads':
					numeric_type = 2;
					break;
				case 'customers':
					numeric_type = 12;
					break;
				default:
					numeric_type = 3;
			}

			if (_.contains(['ccard', 'comcard', 'lcard', 'cucard'], area)) {
				list.push({
					"id": entity.id,
					"type": numeric_type,
					"phones": phl
				});
				return;
			}

			if (type === 'leads') {
				_.each(deals_ct, function (contact) {
					if (!_.contains(contact.linked_leads_id, entity.id)) {
						return;
					}
					custom_field = _.find(contact.custom_fields, function (item) {
						return item.code === 'PHONE';
					});
				});
			} else if (type === 'customers') {
				_.each(customers_ct, function (contact) {
					if (!_.contains(contact.linked_customers_id, entity.id)) {
						return;
					}
					custom_field = _.find(contact.custom_fields, function (item) {
						return item.code === 'PHONE';
					});
				});
			} else {
				custom_field =_.find(entity.custom_fields, function (item) {
					return item.code === 'PHONE';
				});
			}

			if (!_.isUndefined(custom_field)) {
				phones = _.map(custom_field.values, function (value) {
					return value.value.replace(/[^0-9]/ig, "");
				});

				phones = _.filter(phones, function (phone) {
					return _.find(phl, function (number) {
						return number === phone;
					});
				});

				if (phones.length > 0) {
					list.push({
						"id": entity.id,
						"type": numeric_type,
						"phones": phones
					});
				}
			}
		});

		return list;
	};

	this.get_elements_info = function (ids, type) {
		var elem_data;

		$.ajax({
			type: 'GET',
			url: '/private/api/v2/json/' + type + '/list',
			data: ids,
			dataType: 'json',
			async: false,
			success: function (data) {
				elem_data = data;
			}
		});

		return elem_data;
	};

	this.create_sms_list = function (area) {
		var settings = self.get_settings();
		var list = [], ids = ['', ''], type = ['contacts', 'company'], el_id = '';

		if (area == 'clist') {
		
			for (i = 0; i < settings.c_data.length; i++)
				if (settings.c_data[i].type == 'contact') {
					ids[0] += 'id[]=' + settings.c_data[i].id + '&';
				} else {
					ids[1] += 'id[]=' + settings.c_data[i].id + '&';
				}

			for (i = 0; i < 2; i++)
				if (ids[i]) {
					raw_data = self.get_elements_info(ids[i], type[i]);
					list = list.concat(self.get_raw_list(raw_data, type[i]));
				}
		}
		else if (area == 'llist') {
			for (i = 0; i < settings.c_data.length; i++)
				el_id += 'id[]=' + settings.c_data[i].id + '&';

			raw_data = self.get_elements_info(el_id, 'leads');
			list = self.get_raw_list(raw_data, 'leads');
		} else if (area === 'culist') {
			_.each(settings.c_data, function (item) {
				el_id += 'filter[id][]=' + item.id + '&';
			});

			raw_data = self.get_elements_info(el_id, 'customers');
			list = self.get_raw_list(raw_data, 'customers');
		}
		else {
			el_id = document.location.href.split('detail/');
			el_id = el_id[1].split(/[\?#]/);

			switch(area) {
				case 'ccard':
					cart = 'contacts';
					break;
				case 'comcard':
					cart = 'company';
					break;
				case 'cucard':
					cart = 'customers';
					break;
				default:
					cart = 'leads';
			}

			if (el_id[0]) {
				raw_data = cart === 'customers' ? self.get_elements_info('filter[id][]=' + el_id[0], cart)
					: self.get_elements_info('id[]=' + el_id[0], cart);
				list = self.get_raw_list(raw_data, cart);
			}
		}

		return list;
	};
	
	this.send_sms = function (phones, settings, lang) {
        var list = [], 
			area = self.system().area, 
			system = self.system(),
			date = $('#smsru_sending_input').val().replace(/([a-zA-Zа-яА-ЯёЁ])+(:\s)+/ig, ""),
			date_format = AMOCRM.system.format.date.full,
			date_utc = moment(date,date_format).format('X'),
			//endpoint_url = '/widgets/' + system.subdomain + '/loader/' + self.params.widget_code +
				//'/send_sms?amouser=' + system.amouser + '&amohash=' + system.amohash,
			//phones = $('.smsru_cont_phone'), 
			phlist_arr = [];
		
		
		$.each(phones, function (i, item) {
			var value = item.trim().replace(/[^0-9]/ig, "");
			if (value) {
				phlist_arr.push(value);
			}
		});
		
		data = {
			api_id: settings.api_key,
			from: $('div#smsru_ss_cr').find('li[class~="control--select--list--item-selected"]').text(),
			time: date_utc,
			translit: $('#smsru_trans').is(':checked') ? 1 : 0,
			entity: []
		};

		$('#smsru_err').empty();
		$('#smsru_err').attr('style', 'color: red');

		if (area === 'clist' || area === 'llist' || area === 'culist') {
			list = self.create_sms_list(area);
			_.each(list, function (item) {
				data.entity.push({
					numeric_type: item.type,
					id: item.id,
					to: item.phones
				});

				phlist_arr.push(item.phones);
			});
			data.text = $('#smsru_smsmes').val();
		} else {
			data.text = $('#smsru_smsmes').val();
			data.entity.push({
				numeric_type: AMOCRM.constant('card_element_type'),
				id: AMOCRM.constant('card_id'),
				to: phlist_arr
			});
		}

		console.log(data);

		//$('#smsru_sendbtn').trigger('button:load:start');
		
		/*
		$.ajax({
			type: "POST",
			url: endpoint_url,
			data: data,
			dataType: 'json',
			success: function (response) {
				$('#smsru_sendbtn').trigger('button:load:stop');
				
				if (Fn.hasKeys(response, ['error']) && response.error.length) {
					response.error = _.isArray(response.error) ? response.error : [response.error];
					_.each(response.error, function (error) {
						AMOCRM.inbox.showNotification({
							title: lang.sendSmsError,
							type: 'error',
							icon: self.params.path + '/images/inbox_error.png',
							message: error
						});
					});
				} else if (Fn.hasKeys(response, ['status']) && response.status) {
					$('#smsru_err').attr('style', 'color: green');
					$('#smsru_err').text( lang.msgOk);
					if (_.contains(['ccard', 'comcard', 'lcard', 'cucard'], area)) {
						location.reload(true);
					}
				} else {
					AMOCRM.inbox.showNotification({
						title: lang.sendSmsError,
						type: 'error',
						icon: self.params.path + '/images/inbox_error.png',
						message: lang.sendSmsErrorSmthWrong
					});
				}
			}
		});
				*/
	};
	
	this.loadTeamplateToEditor = function (cur_tpl) {
        $('#smsru_tpname').val(self.htmlspecialchars(cur_tpl.template_name, true));
        $('#smsru_tpcontent').val(self.htmlspecialchars(cur_tpl.template_text, true));
	};
	
	this.htmlspecialchars = function (html, revers) {
		if (revers) {
			html = html.replace(/&lt;/g, '<');
			html = html.replace(/&gt;/g, '>');
			html = html.replace(/&quot;/g, '"');
		} else {
			html = html.replace(/</g, '&lt;');
			html = html.replace(/>/g, '&gt;');
			html = html.replace(/"/g, '&quot;');
		}

		return html;
	};
	
	this.getXmlHttp = function() {
		var xmlhttp;
		try {
			xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (E) {
				xmlhttp = false;
			}
		}
		if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
			xmlhttp = new XMLHttpRequest();
		}
		return xmlhttp;
	};
	
	this.get_templates = function() {
		var settings = self.get_settings();
		$.ajax({
			type: 'GET',
			url: 'https://sms.ru/amocrm_templates/get?api_id='+settings.api_key+'&json=1',
			success: function (data) {
				if(data.status === "OK" && data.templates){
					var templates = []
					$.each(data.templates, function(i, item){
						templates.push({
							tpid: i,
							template_name: item.template_name,
							template_text: item.template_text,
						});
					});
					self.set_settings({templates: templates});
				}
			}
		});
	};
	
	this.templates_editor = function (settings, lang) {
		var tpid = 0,
			temp_tpl = self.get_settings().templates || [],  
			macros_block = '', 
			data,
			macros_tpl = '<div class="sms_ru_tpl_markers">' +
				'<div class="sms_ru_tpl_markers__title">' +
				'{{ macros_title }}' +
				'</div>' +
				'{% for item in macroses %}' +
				'<p class="marker-list__row"><span class="marker-list__tag-bot" data-marker="{{ item.macros }}">' +
				'<span class="marker-list__tag">{{ item.macros }}</span></span>' +
				'<span class="marker-list__tag-descr"> - {{ item.title }} </span></p>' +
				'{% endfor %}' +
				'</div>',
			tp_tpl = '{% for tpl in templates %}' +
				'<div class="template-item__wrapper clearfix" smsru_tpid="{{ loop.index0 }}">' +
				'<div class="template-item__inner">' +
				'<div class="template-item__body clearfix">' +
				'<div class="template-item__body-inner">' +
				'<div class="template-item__name">{{ tpl.template_name }}</div></div>' +
				'<div id="smsru_tpremove" class="template-item__body-edit">' +
				'<span class="icon icon-inline icon-delete-trash"></span>' +
				'</div></div></div></div>' +
				'{% endfor %}',
			before_macros = '<div id="smsru_tp_details">' +
				self.render(
				  {ref: '/tmpl/controls/input.twig'},
				  {
					id: 'smsru_tpname',
					name: 'smsru_tpname',
					placeholder: lang.templateTitle
				  }
				) +
				self.render(
				  {ref: '/tmpl/controls/textarea.twig'},
				  {
					id: 'smsru_tpcontent',
					name: 'smsru_tpcontent',
					class_name: 'custom-scroll'
				  }
				) +
				'<div id="smsru_tp_macroses" class="custom-scroll">',
			after_macros = '</p>' +
				'</div>' +
				'</div>' +
				'<div id="smsru_tp_list">' +
				'<div id="sms-templates_wrapper">' +
				'<div id="sms-templates_header">' + lang.smsTemplates + '</div>' +
				'<div class="sms-templates_holder">' +
				'<div id = "smsru_sortable" class="smsru_template-item">' +
					self.render({data: tp_tpl}, {templates: temp_tpl}) +
				'</div>' +
				'</div>' +
				self.render(
				  {ref: '/tmpl/controls/button.twig'},
				  {
					id: 'smsru_addtp',
					text: '<span class="icon icon-inline button-input-add-icon icon-tag-plus-dark"></span>' + lang.addTemplate
				  }
				) +
				'</div>' +
				self.render(
				  {ref: '/tmpl/controls/button.twig'},
				  {
					id: 'smsru_savetps',
					class_name: 'button-input_blue',
					text: lang.saveTemplates,
					disabled: true
				  }
				) +
			'</div>';
		
        if (_.contains(['leads', 'leads-dp'], AMOCRM.data.current_entity)) {
          macros_block += self.render({data: macros_tpl}, {macroses: settings.leads_macros, macros_title: lang.leadsMacroses})
        }
        if (_.contains(['customers', 'customers-dp'], AMOCRM.data.current_entity)) {
          macros_block += self.render({data: macros_tpl}, {macroses: settings.customers_macros, macros_title: lang.customersMacroses})
        }
        if (_.contains(['leads', 'leads-dp', 'customers', 'customers-dp', 'contacts'], AMOCRM.data.current_entity)) {
          macros_block += self.render({data: macros_tpl}, {macroses: settings.contacts_macros, macros_title: lang.contactsMacroses})
        }
        macros_block += self.render({data: macros_tpl}, {macroses: settings.companies_macros, macros_title: lang.companyMacroses})

        data = before_macros+macros_block+after_macros;

        modal = new Modal({
			class_name: 'smsru-modal-window',
			can_centrify: !1,
			can_destroy: !1,
			disable_overlay_click: !0,
			init: function ($modal_body) {
				var $this = $(this);
				$this.addTpl = function () {
					if (temp_tpl.length != 0) {

						self.loadTeamplateToEditor(temp_tpl[tpid]);
						$('#smsru_tp_list .template-item__wrapper[smsru_tpid = ' + tpid + ']').addClass('tpl-active');
					
					} 

					tpid = temp_tpl.length;
					temp_tpl.push({tpid: tpid, template_name: 'Новый шаблон', template_text: ''});
	
					$('.tpl-active').removeClass('tpl-active');
					$('.smsru_template-item').append('<div class="template-item__wrapper clearfix tpl-active" smsru_tpid="' + tpid + '">' +
						'<div class="template-item__inner">' +
						'<div class="template-item__body clearfix">' +
						'<div class="template-item__body-inner">' +
						'<div class="template-item__name">' + temp_tpl[tpid].template_name + '</div>' +
						'</div>' +
						'<div id="smsru_tpremove" class="template-item__body-edit">' +
						'<span class="icon icon-inline icon-delete-trash"></span>' +
						'</div>' +
						'</div>' +
						'</div>' +
					'</div>');
					self.loadTeamplateToEditor({tpid: tpid, template_name: 'Новый шаблон', template_text: ''});
				};
				$modal_body
				  .trigger('modal:loaded')
				  .html(data)
				  .trigger('modal:centrify')
				  .append('<span class="modal-body__close"><span class="icon icon-modal-close"></span></span>');
				
				$( function() {
					$( "#smsru_sortable" ).sortable({
						revert: true
					});
				});
				
				$("#smsru_sortable").on( "sortstop", function( event, ui ) {
					$('#smsru_savetps').trigger('button:save:enable')
				});
				
				$(document).on('click' + self.ns, '#smsru_tpremove', function () {
					var del_tpid = $(this).closest("div.template-item__wrapper").attr('smsru_tpid');

					temp_tpl.splice(del_tpid, 1);

					$(this).closest("div.template-item__wrapper").nextAll().map(function (indx, element) {
						$(element).attr('smsru_tpid', $(element).attr('smsru_tpid') - 1);
					});
					$(this).closest("div.template-item__wrapper").remove();

					if (del_tpid <= tpid) {
						if (del_tpid != tpid) {
							$('#smsru_tp_list .tpl-active').removeClass('tpl-active');
						}
						if (tpid != 0) {
							tpid--;
						}
						$('#smsru_tp_list .template-item__wrapper[smsru_tpid = ' + tpid + ']').addClass('tpl-active');
						if (temp_tpl.length != 0) {
							self.loadTeamplateToEditor(temp_tpl[tpid]);
						} else {
							self.loadTeamplateToEditor({template_name: '', template_text: ''});
						}
					}
					$('#smsru_savetps').trigger('button:save:enable');
				});
				
				$(document).on('click' + self.ns, '#smsru_addtp', function () {
					$this.addTpl();
					$('#smsru_savetps').trigger('button:save:enable')
				});
				
				$(document).on('click' + self.ns, '.sms-templates_holder .template-item__body-inner', function () {
					$('.tpl-active').removeClass('tpl-active');
					tpid = $(this).closest("div.template-item__wrapper").attr('smsru_tpid');
					$('#smsru_tp_list .template-item__wrapper[smsru_tpid = ' + tpid + ']').addClass('tpl-active');
					self.loadTeamplateToEditor(temp_tpl[tpid]);
					$('#smsru_savetps').trigger('button:save:enable')
				});
				
				$(document).on('click' + self.ns, '#smsru_tp_macroses .marker-list__tag', function () {
					if (temp_tpl.length == 0) {
						$this.addTpl();
					}
					self.insertTextAtCursor(document.getElementById('smsru_tpcontent'), $(this).text());
					temp_tpl[tpid].template_text = self.htmlspecialchars($('#smsru_tpcontent').val());
					$('#smsru_tpcontent').focus();
					$('#smsru_savetps').trigger('button:save:enable')
				});
				
				$(document).on('click' + self.ns, '#smsru_savetps', function () {
					var tpls = [],  
						
					tp_tpl = '{% for tpl in templates %}' +
					'<div class="template-item__wrapper clearfix" smsru_tpid="{{ loop.index0 }}">' +
					'<div class="template-item__inner">' +
					'<div class="template-item__body clearfix">' +
					'<div class="template-item__body-inner">' +
					'<div class="template-item__name">{{ tpl.template_name }}</div></div>' +
					'<div id="smsru_tpremove" class="template-item__body-edit">' +
					'<span class="icon icon-inline icon-delete-trash"></span>' +
					'</div></div></div></div>' +
					'{% endfor %}';
					
					var temp_tpl_ids = [];
					var temp_tpl_list = $("#smsru_sortable").find(".template-item__wrapper");
					$.each(temp_tpl_list, function(i, item){
						if($(item).attr('smsru_tpid')){
							temp_tpl_ids.push($(item).attr('smsru_tpid'));
						}
					});
					
					var new_temp_tpl = [];
					$.each(temp_tpl_ids, function(i, id){
						$.each(temp_tpl, function(j, item){
							if(id == item.tpid){
								new_temp_tpl.push({
									tpid: i,
									template_name: item.template_name,
									template_text: item.template_text,
								});
							}
						});
					});
					
					temp_tpl = new_temp_tpl;

					self.set_settings({templates: temp_tpl});
					
					$.ajax({
						type: 'POST',
						dataType: 'json',
						url: 'https://sms.ru/amocrm_templates/replace?api_id='+settings.api_key+'&json=1',
						data: {
							templates: JSON.stringify(temp_tpl)
						},
						success: function (data) {
							console.log(data);
						}
					});
					
					$('.smsru_template-item').html(self.render({data: tp_tpl}, {templates: temp_tpl}));
					$('#smsru_tp_list .template-item__wrapper[smsru_tpid = ' + tpid + ']').addClass('tpl-active');

					AMOCRM.widgets.clear_cache();
					
					settings = self.get_settings();

					if (temp_tpl.length != 0) {
						tpls.push({option: lang.templates, id: -2});
						for (i = 0; i < settings.templates.length; i++) {
						  tpls.push({option: settings.templates[i].template_name, id: i});
						}
					} else {
						tpls.push({option: lang.noTemplates, id: -2});
					}

					tpls.push({option: lang.editClick, class_name: 'smsru_edit_templates', id: -1});

					$('#smsru_wrap_tpl').html(self.render(
						{ref: '/tmpl/controls/select.twig'},
						{
						  items: tpls,
						  id: 'smsru_templates'
						}
					));
					
					$(this).trigger('button:saved');
				});
				
				$(document).on('keyup' + self.ns, '#smsru_tpname', function () {
					if (temp_tpl.length == 0) {
						$this.addTpl();
					}
					temp_tpl[tpid].template_name = self.htmlspecialchars($('#smsru_tpname').val());
					$('#smsru_savetps').trigger('button:save:enable')
				});
				
				$(document).on('keyup' + self.ns, '#smsru_tpcontent', function () {
					if (temp_tpl.length == 0) {
						$this.addTpl();
					}
					temp_tpl[tpid].template_text = self.htmlspecialchars($('#smsru_tpcontent').val());
				});
				
				$('#smsru_savetps').trigger('button:save:disable')
			},

			destroy: function () {
				$(document).off('click' + self.ns, '#smsru_tpremove');
				$(document).off('click' + self.ns, '#smsru_addtp');
				$(document).off('click' + self.ns, '.sms-templates_holder .template-item__body-inner');
				$(document).off('click' + self.ns, '#smsru_tp_macroses .marker-list__tag');
				$(document).off('click' + self.ns, '#smsru_savetps');
				$(document).off('keyup' + self.ns, '#smsru_tpname');
				$(document).off('keyup' + self.ns, '#smsru_tpcontent');
				$("#smsru_sortable").off( "sortstop");
			}
        });
	};
	
	this.create_macros_names = function (entity_type, entity_fields) {
        var lang = self.i18n('userLang'),
			ms = [],
			default_ms = [
				{
					macros: '{{'+entity_type+'.responsible_user}}',
					title: lang.macrosResp
				},
				{
					macros: '{{'+entity_type+'.responsible_user_phone}}',
					title: lang.macrosRespPhone
				},
				{
					macros: '{{'+entity_type+'.responsible_user_email}}',
					title: lang.macrosRespMail
				},
				{
					macros: '{{'+entity_type+'.id}}',
					title: 'Id'
				}
			];

        _.each(entity_fields, function (field) {
			ms.push({
				macros: '{{'+entity_type+'.cf.'+field.id+'}}',
				title: field.name
			});
        });
        default_ms = default_ms.concat(ms);

        return default_ms;
	};
	
	this.get_senders = function (settings, lang, selected) {
        self.crm_post(
			'https://sms.ru/my/senders',
			{
				api_id: settings.api_key
			},
			function (msg) {
				var sel_resp = [], options;
				resp = msg.trim().split("\n");

				if (resp[0] != 100) {
					$('#smsru_err').text(lang.getSrError + '!');
				} else {
					$('#smsru_err').text('');
					for (i = 1; i < resp.length; i++)
						sel_resp.push({
							option: resp[i],
							id: resp[i]
						});
					options = {items: sel_resp, id: 'smsru_senders'};
					if (!_.isUndefined(selected) && $.inArray(selected, resp) !== -1) {
						options.selected = selected;
					}
					$('#smsru_ss_cr').empty();
					$('#smsru_ss_cr').append(self.render(
						{ref: '/tmpl/controls/select.twig'},
						options
					));

					document.cookie = 'smsru-senders=' + encodeURIComponent(msg) + '; path=/';
					$('#smsru_ss_cr .control--select--button')
					.text(
						self.i18n('userLang').senderName + ': ' + $('#smsru_ss_cr .control--select--list--item-selected').attr('data-value')
					);
					if (_.isUndefined(selected) || selected === '') {
						$('#smsru_senders').change();
					}
				}
			},
			'text',
			function (XMLHttpRequest) {
				$('#smsru_err').text(lang.msgError + ' ' + lang.responseError);
				return false;
			}
        );
    };

	this.callbacks = {
		spiner: '<div id="amocrm-spinner" style="both:clear;"><span style="width: 20px;height: 20px;margin: 0 auto;display: block;position: static;" class="pipeline_leads__load_more__spinner spinner-icon spinner-icon-abs-center"></span></div>',
		modalForm: null,
		initSettings: function(){
			return window.youplatformSMSRU.execute('initSettings', self);
		},
		initReport: function(){
			return window.youplatformSMSRU.execute('initReport', self);
		},
		initSignatures: function(){
			return window.youplatformSMSRU.execute('initSignatures', self);
		},
		initReg: function(){
			return window.youplatformSMSRU.execute('initReg', self);
		},
		render: function(){
			return window.youplatformSMSRU.execute('render', self);
		},
		init: function(){
			return window.youplatformSMSRU.execute('init', self);
		},
		bind_actions: function(){
			return window.youplatformSMSRU.execute('bind_actions', self);
		},
		settings: function(){
			return window.youplatformSMSRU.execute('settings', self);
		},
		onSave: function(){
			return window.youplatformSMSRU.execute('onSave', self);
		},
		dpSettings: function(){
			return window.youplatformSMSRU.execute('dpSettings', self);
		},
		destroy: function(){
			return window.youplatformSMSRU.execute('destroy', self);
		},
		contacts: {
			selected: function(){
				return window.youplatformSMSRU.execute('contacts', self);
			}
		},
		leads: {
			selected: function(){
				return window.youplatformSMSRU.execute('leads', self);
			}
		},
		tasks: {
			selected: function(){
				return window.youplatformSMSRU.execute('tasks', self);
			}
		}
	};
	return this;
};
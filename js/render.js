window.youplatformSMSRU.render.push(
    function(self){
        var w_code = self.get_settings().widget_code,
            area = AMOCRM.getWidgetsArea(),
			twig = require('twigjs'),
			lang = self.i18n('userLang'), 
			settings = self.get_settings(), 
			senders = [], 
			cd = new Date,
			tpls = [];
			
		self.get_templates();	
		
		//$('head').append('<link rel="stylesheet" href="' + self.params.path + '/css/smsru_select2.css">');
        //$("head").append('<link type="text/css" rel="stylesheet" href="' + self.params.path + '/css/smsru_style.css?' + Date.now() + '">');
		$('head').append('<link rel="stylesheet" href="'+ self.getServerUrl() +'/css/smsru_select2.css">');
        $("head").append('<link type="text/css" rel="stylesheet" href="' + self.getServerUrl() +'/css/smsru_style.css?' + Date.now() + '">');
		
		//console.log(self.ns);
		
		var smsru_templates = [
			{
				id: 0,
				option: 'Выберите шаблон'
			}
		];
		
		smsru_templates.push({
            option: 'Редактор шаблонов',
            class_name: 'smsru_edit_templates',
            id: -1
        });
		
		var smsru_senders = [
			{
				id: 0,
				option: 'Выберите отправителя'
			}
		];
		
		var time_items = [
			{value: '00:00'}, {value: '00:30'}, {value: '01:00'}, {value: '01:30'},
			{value: '02:00'}, {value: '02:30'}, {value: '03:00'}, {value: '03:30'},
			{value: '04:00'}, {value: '04:30'}, {value: '05:00'}, {value: '05:30'},
			{value: '06:00'}, {value: '06:30'}, {value: '07:00'}, {value: '07:30'},
			{value: '08:00'}, {value: '08:30'}, {value: '09:00'}, {value: '09:30'},
			{value: '10:00'}, {value: '10:30'}, {value: '11:00'}, {value: '11:30'},
			{value: '12:00'}, {value: '12:30'}, {value: '13:00'}, {value: '13:30'},
			{value: '14:00'}, {value: '14:30'}, {value: '15:00'}, {value: '15:30'},
			{value: '16:00'}, {value: '16:30'}, {value: '17:00'}, {value: '17:30'},
			{value: '18:00'}, {value: '18:30'}, {value: '19:00'}, {value: '19:30'},
			{value: '20:00'}, {value: '20:30'}, {value: '21:00'}, {value: '21:30'},
			{value: '22:00'}, {value: '22:30'}, {value: '23:00'}, {value: '23:30'}
		];

		var sending_input_val = lang.sending + ': ' + self.addZero(cd.getDate()) + '.' + self.addZero(cd.getMonth() + 1) + '.' + self.addZero(cd.getFullYear()) + ' ' + self.addZero(cd.getHours()) + ':' + self.addZero(cd.getMinutes());
		
		var balans = '500';
		
		self.getTemplate('render', function (template) {
			self.render_template({
				caption: {
					class_name: 'js-ac-caption',
					html: ''
				},
				body: template.render({
					balans: balans,
					templates: self.render_select('smsru_templates', smsru_templates, '', '', 'smsru_templates'),
					textarea: self.render_textarea('smsru_textarea', lang.textSMS + '&hellip;', 'smsru-textarea', 'smsru_smsmes'),
					translit: self.render_checkbox('smsru_trans', 'Транслит', false, 'smsru_trans', 'smsru_trans'),
					senders: self.render_select('smsru_senders', smsru_senders, '', '', ''),
					sending: self.render_input('smsru_sending_input', '', sending_input_val, '', 'smsru_sending_input', true),
					date: self.date_field('smsru_date', 'smsru_sending_date'),
					time: self.render_suggest(time_items, 'smsru_time', 'smsru_sending_time'),
					button: self.render_button('', lang.textButton, '', 'smsru_sendbtn'),
					lang: {
						date: lang.date,
						time: lang.time,
					}
				}),
				render: ''
			});
			
			$('#smsru-multiple-select').select2({
				placeholder: "Список получателей",
				data: self.getContactList(),
				tags: true,
				width: '100%',
			});

			$("#smsru-multiple-select").val(self.selectAllContacts()).trigger("change");
			$('#smsru-multiple-select').parent().find('.select2-selection__rendered').addClass('custom-scroll');
			
			$('.multiple-select').on('select2:open', function () {
				$('.select2-selection__rendered, .select2-results__options').addClass('custom-scroll');
			});
			
			$('.card-widgets__widget[data-code="' + w_code + '"] .js-widget-caption-block').css('background-color', '#6a6a6a');
			$('.card-widgets__widget[data-code="' + w_code + '"] .js-widget-caption-block').css('padding', '5px');
			$('.card-widgets__widget[data-code="' + w_code + '"] .card-widgets__widget__body').css({
				"background-color": "#fff",
				"padding": "1px 15px 15px 15px"
			});
		});
		
		
		/*
		var template = '<div class="smsru-form">' +
			'<div class="smsru-form-margin">' +
				'Баланс: <span id = "smsru-balans">500</span>' +
			'</div>' +
			'<div class="smsru-form-margin">' +
				self.render_select('smsru_templates', smsru_templates, '', '') +
			'</div>' +
			'<div class="smsru-form-margin">' +
				self.render_textarea('smsru_textarea', lang.textSMS + '&hellip;', 'smsru-textarea', '') +
			'</div>' +
			'<div class="smsru-form-margin">' +
				self.render_select('smsru_senders', smsru_senders, '', '') +
			'</div>' +
			'<div class="smsru-form-margin">' +
				'<select id = "smsru-multiple-select" class="smsru-multiple-select" multiple="" tabindex="-1" aria-hidden="true"></select>' +
			'</div>' +
			'<div class="smsru-form-margin smsru_elem smsru_sending" id ="smsru_sending_wrapper">' +
				'<div class="smsru_sending_field">' +
					self.render_input('smsru_sending_input', '', sending_input_val, '', 'smsru_sending_input', true) +
				'</div>' +
				'<div class="smsru_sending_popup">' +
					'<div class="smsru_sending_popup_row">' +
						'<label class="smsru_sending_popup_label" for="smsru_date">' + lang.date + '</label>' +
						'<div class="smsru_sending_popup_input">' +
							self.date_field('smsru_date', 'smsru_sending_date') +
						'</div>'+
					'</div>' +
					'<div class="smsru_sending_popup_row">' +
						'<label class="smsru_sending_popup_label" for="smsru_time">' + lang.time + '</label>' +
						'<div class="smsru_sending_popup_input">' +
							self.render_suggest(time_items, 'smsru_time', 'smsru_sending_time') +
						'</div>' +
					'</div>' +
				'</div>' +
            '</div>' +
			'<div class="smsru-form-margin">' +
				self.render_button('', lang.textButton, '', 'smsru_sendbtn') +
			'</div>' +
			'<div class="smsru-form-margin">' +
				'<div id="smsru_err">'+ lang.err207 +'</div>' +
			'</div>' +
		'</div>';
		
		self.render_template({
			caption: {
				class_name: 'js-ac-caption',
				html: ''
			},
			body: template,
			render: ''
		});
		
		
		$('#smsru-multiple-select').select2({
            placeholder: "Список получателей",
            //data: self.getContactList(),
            data: [],
            tags: true,
            width: '100%',
        });

		
        //var allContacts = self.selectAllContacts();
        //$("#smsru-multiple-select").val(allContacts).trigger("change");

        $('#smsru-multiple-select').parent().find('.select2-selection__rendered').addClass('custom-scroll');
		
        $('.multiple-select').on('select2:open', function () {
            $('.select2-selection__rendered, .select2-results__options').addClass('custom-scroll');
        });
		
		$('.card-widgets__widget[data-code="' + w_code + '"] .js-widget-caption-block').css('background-color', '#6a6a6a');
		$('.card-widgets__widget[data-code="' + w_code + '"] .js-widget-caption-block').css('padding', '5px');
        $('.card-widgets__widget[data-code="' + w_code + '"] .card-widgets__widget__body').css({
            "background-color": "#fff",
            "padding": "1px 15px 15px 15px"
        });
		
		*/
        return true;
    }
);
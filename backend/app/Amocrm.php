<?php

class Amocrm
{
    
    public $setting = array();
	
    public $authUrl = '/private/api/auth.php';
	
    private $_methods = [
		'contacts' => '/private/api/v2/json/contacts/list',
		'notes_set' => '/private/api/v2/json/notes/set',
		'links' => '/private/api/v2/json/links/list',
		'customers' => '/private/api/v2/json/customers/list',
		'leads' => '/private/api/v2/json/leads/list',
		'widgets' => '/private/api/v2/json/widgets/list',
		'widget_set' => '/private/api/v2/json/widgets/set',
	];
	private $_curl = FALSE;
	
	private $_response;

    public function setSetting($array){
        $this->setting = $array;
    }

    /**
     * Подготовка ссылки для отправки и/или получения данных
     *
     * @param string $url
     * @param string $type
     * @return string
     */
    public function prepareLink($url = '', $type = 'json')
    {
        if (count($this->setting) > 0) {
            $link = 'https://' . $this->setting['subdomain'] . '.amocrm.ru'  . $url;
            if ($type == 'json') {
                $link .= '?type=json';
            }

            return $link;
        }
        return '';
    }

    /**
     * Авторизация
     */
    public function auth()
    {
        $link = $this->prepareLink($this->authUrl);
        $post = array(
            'USER_LOGIN' => $this->setting['amo_login'],
            'USER_HASH' => $this->setting['amo_hash'],
        );
        $result = $this->sendCURL($link, $post);
        if ($result['code'] == '200') {
            return $result['out'];
        }
        return false;
    }

    /**
     * Получаем данные по аккаунту
     */
    public function account()
    {
        $link = $this->prepareLink('/private/api/v2/json/accounts/current', false);
        $post = array();
        $result = $this->sendCURL($link, $post, 'GET');
        return $result["out"]->account;
    }

    /**
     * Создаем сделку
     */
    public function addLead($data = array())
    {
        $data = array(
            'request' => array(
                'leads' => array(
                    'add' => array(
                        $data
                    )
                )
            )
        );
        $link = $this->prepareLink('/private/api/v2/json/leads/set', false);
        if ($result = $this->sendCURL($link, $data)) {
            return $result['out']->leads->add[0]->id;
        } else {
            return null;
        }
    }

    /**
     * Создаем контакт
     */
    public function addContact($data = array())
    {
        $data = array(
            'request' => array(
                'contacts' => array(
                    'add' => array(
                        $data
                    )
                )
            )
        );
        $link = $this->prepareLink('/private/api/v2/json/contacts/set', false);
        if ($result = $this->sendCURL($link, $data)) {
            return $result['out']->contacts->add[0]->id;
        } else {
            return null;
        }
    }

    /**
     * Создаем примечания
     */
    public function addNotes($data = array())
    {
        $data = array(
            'request' => array(
                'notes' => array(
                    'add' => $data
                )
            )
        );
        $link = $this->prepareLink('/private/api/v2/json/notes/set', false);
        if ($result = $this->sendCURL($link, $data)) {
            return $result['out']->notes->add;
        } else {
            return null;
        }
    } 
	
	/**
     * Получаем примечания
     */
    public function getNotes($type, $id = false, $element_id = false, $note_type = false, $limit_offset = false, $date_start = false)
    {
        $str = "type=".$type;
		if($id) $str .= '&id[]='.$id;
		if($element_id) $str .= '&element_id='.$element_id;
		if($note_type) $str .= '&note_type='.$note_type;
		if($limit_offset !== false) $str .= '&limit_rows=500&limit_offset='.$limit_offset;
		
        $link = $this->prepareLink('/api/v2/notes?'.$str, false);
        if ($result = $this->sendCURLv2($link, [], 'GET', $date_start)) {
            return $result['out'];
        } else {
            return null;
        }
    }
	
	public function getAllNotes($type, $id = false, $element_id = false, $note_type = false, $date_start = false)
	{
		$allNotes = array();
		$limit_offset = 0;
		$c = true;
	
		while($c != null){
			$c = $this->getNotes($type, $id, $element_id, $note_type, $limit_offset, $date_start);
			if($c != null){
				foreach($c as $val) { 
					$allNotes[] = $val;
				}
				$limit_offset = $limit_offset + 500;
				sleep(1);
			}
			else break;
		}	
		if(!empty($allNotes)){
			return $allNotes;
		} else {
			return null;
		}
	}
	
	/**
     * Получаем сделки из указанных статусов по ответственному
     */
    public function getLeadsByStatusAndResp($array = [], $responsible_user_id, $date_start, $limit_offset)
    {
        $str = "";

        $i = 0;
        foreach($array as $id){
            if($i == 0){
                $str .= 'status[]='.$id;
            } else {
                $str .= '&status[]='.$id;
            }
            $i++;
        }

        $link = $this->prepareLink('/private/api/v2/json/leads/list?'.$str.'&responsible_user_id='.$responsible_user_id.'&limit_rows=500&limit_offset='.$limit_offset, false);
        
		if ($result = $this->sendCURL($link, [], 'GET', $date_start)) {
            return $result['out']->leads;
        } else {
            return null;
        }
    }

    /**
     * Проверяем контакт
     */
    public function checkContact($query)
    {
        $link = $this->prepareLink('/private/api/v2/json/contacts/list?query='.$query, false);
        $post = array();
        $result = $this->sendCURL($link, $post, 'GET');
        return $result["out"]->contacts[0]->id;
    }

    /**
     * Обновляем контакт
     */
    public function updateContact($data = array())
    {
        $data = array(
            'request' => array(
                'contacts' => array(
                    'update' => array(
                        $data
                    )
                )
            )
        );
        $link = $this->prepareLink('/private/api/v2/json/contacts/set', false);
        if ($result = $this->sendCURL($link, $data)) {
            return $result['out']->contacts->update[0];
        } else {
            return null;
        }
    }

    /**
     * Обновляем задачу
     */
    public function updateTask($data = array())
    {
        $data = array(
            'request' => array(
                'tasks' => array(
                    'update' => $data
                )
            )
        );

        $link = $this->prepareLink('/private/api/v2/json/tasks/set', false);
        if ($result = $this->sendCURL($link, $data)) {
            return $result['out'];
        } else {
            return null;
        }
    }

    /**
     * Получаем все контакты
     */
    public function getAllContacts($limit_offset = null)
    {
        $link = $this->prepareLink('/private/api/v2/json/contacts/list?limit_rows=500&limit_offset='.$limit_offset, false);
        $post = array();
        $result = $this->sendCURL($link, $post, 'GET');
        return $result["out"]->contacts;
    }

    /**
     * Получаем все компании
     */
    public function getAllCompanies($limit_offset = null)
    {
        $link = $this->prepareLink('/private/api/v2/json/company/list?limit_rows=500&limit_offset='.$limit_offset, false);
        $post = array();
        $result = $this->sendCURL($link, $post, 'GET');
        return $result["out"]->contacts;
    }
    
    /**
     * Получаем сделки (Тестовый метод)
     */
    public function getLeads()
    {
        $link = $this->prepareLink('/private/api/v2/json/leads/list', false);
        $post = array();
        $result = $this->sendCURL($link, $post, 'GET');
        return $result["out"];
    }

    /**
     * Получаем сделку по id
     */
    public function getLeadById($ids)
    {
		if(isset($ids["id"])){
			$list = $ids["id"];
		} else $list = $ids;
		$str = "";
		for($i = 0; $i < count($list); $i++){
			if($i == 0){
				$str .= "?id[]=".$list[$i];
			} else {
				$str .= "&id[]=".$list[$i];
			}
		}
		
        $link = $this->prepareLink('/private/api/v2/json/leads/list'.$str, false);
        $post = array();
        $result = $this->sendCURL($link, $post, 'GET');
        return $result["out"]->leads;
    }
	
	/**
     * Получаем контакт по id
     */
    public function getContactById($ids)
    {
		if(isset($ids["id"])){
			$list = $ids["id"];
		} else $list = $ids;
		$str = "";
		for($i = 0; $i < count($list); $i++){
			if($i == 0){
				$str .= "?id[]=".$list[$i];
			} else {
				$str .= "&id[]=".$list[$i];
			}
		}
		
        $link = $this->prepareLink('/private/api/v2/json/contacts/list'.$str, false);
        $post = array();
        $result = $this->sendCURL($link, $post, 'GET');
        return $result["out"]->contacts;
    }
	
	/**
     * Получаем все компании
     */
    public function getCompanyById($ids)
    {
		if(isset($ids["id"])){
			$list = $ids["id"];
		} else $list = $ids;
		$str = "";
		for($i = 0; $i < count($list); $i++){
			if($i == 0){
				$str .= "?id[]=".$list[$i];
			} else {
				$str .= "&id[]=".$list[$i];
			}
		}
		
        $link = $this->prepareLink('/private/api/v2/json/company/list'.$str, false);
        $post = array();
        $result = $this->sendCURL($link, $post, 'GET');
        return $result["out"]->contacts;
    }

    /**
     * Получаем сделки из указанных статусов
     */
    public function getLeadsByStatus($array = [], $limit_offset)
    {
        $str = "";

        $i = 0;
        foreach($array as $id){
            if($i == 0){
                $str .= 'status[]='.$id;
            } else {
                $str .= '&status[]='.$id;
            }
            $i++;
        }

        $link = $this->prepareLink('/private/api/v2/json/leads/list?'.$str.'&limit_rows=500&limit_offset='.$limit_offset, false);
        if ($result = $this->sendCURL($link, [], 'GET')) {
            return $result['out'];
        } else {
            return null;
        }
    }

    /**
     * Ищем сделки по полям сущностей
     */
    public function searchLeads($query)
    {
        $link = $this->prepareLink('/private/api/v2/json/leads/list?query='.$query, false);
        $post = array();
        $result = $this->sendCURL($link, $post, 'GET');
        return $result["out"]->leads;
    }

    /**
     * Получаем задачи сделки по её id
     */
    public function getTasksByLeadId($id)
    {
        $link = $this->prepareLink('/private/api/v2/json/tasks/list?element_id[]='.$id, false);
        $post = array();
        $result = $this->sendCURL($link, $post, 'GET');
        return $result["out"]->tasks;
    } 
	
	/**
     * Получаем задачу по её id
     */
    public function getTasksById($id)
    {
        $link = $this->prepareLink('/private/api/v2/json/tasks/list?id[]='.$id, false);
        $post = array();
        $result = $this->sendCURL($link, $post, 'GET');
        return $result["out"]->tasks;
    }
	
	/**
     * Ищем сделки без задач
     */
    public function searchLeadsWithOutTasks($array = [])
    {
		if(!empty($array)){
			$str = "";
			$i = 0;
			foreach($array as $id){
				if($i == 0){
					$str .= 'status[]='.$id;
				} else {
					$str .= '&status[]='.$id;
				}
				$i++;
			}
			$link = $this->prepareLink('/api/v2/leads?'.$str.'&filter[tasks]=1', false);
		} else {
			$link = $this->prepareLink('/api/v2/leads?filter[tasks]=1', false);
		}
		
        $result = $this->send_get_request($link);
		
		return $result["out"];
    } 

    /**
     * Установка связей между сущностями
     */
    public function setLinks($data = array())
    {
        $data = array(
            'request' => array(
                'links' => array(
                    'link' => array(
                        $data
                    )
                )
            )
        );
        $link = $this->prepareLink('/private/api/v2/json/links/set', false);
        if ($result = $this->sendCURL($link, $data)) {
            return $result['out']->links;
        } else {
            return null;
        }
    }

    /**
     * Обновляем сделку
     */
    public function updateLead($data = array())
    {
        $data = array(
            'request' => array(
                'leads' => array(
                    'update' => array(
                        $data
                    )
                )
            )
        );
        $link = $this->prepareLink('/private/api/v2/json/leads/set', false);
        if ($result = $this->sendCURL($link, $data)) {
            return $result['out']->leads->update[0];
        } else {
            return null;
        }
    }
	
	private function get_curl() {
		if (empty($this->_curl)) {
			$this->_curl = curl_init();
			curl_setopt($this->_curl, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($this->_curl, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($this->_curl, CURLOPT_CONNECTTIMEOUT, 2);
			curl_setopt($this->_curl, CURLOPT_TIMEOUT, 2);
			curl_setopt($this->_curl, CURLOPT_FOLLOWLOCATION, TRUE);
			curl_setopt($this->_curl, CURLOPT_HEADER, FALSE);
			curl_setopt($this->_curl, CURLOPT_RETURNTRANSFER, TRUE);
		}

		return $this->_curl;
	}
	
	public function send_api_request($element, $data = NULL, $is_post = FALSE) {
	    if (!isset($this->_methods[$element])) {
			return FALSE;
		}
		//$url = $this->_base_url . $this->_methods[$element] . '?' . http_build_query($this->_user);
		
		$url = $this->prepareLink($this->_methods[$element], 'json');
		
		$this->get_curl();
		if ($is_post) {
			curl_setopt($this->_curl, CURLOPT_POST, TRUE);
			$response = $this->send_request($url, $data, TRUE);
		} else {
			curl_setopt($this->_curl, CURLOPT_POST, FALSE);
			if (is_array($data)) {
				// поиск по параметру ['id' => 123] или ['id' => [123, 456, 789]]
				$query = http_build_query($data);
			} else {
				// Поиск по строке
				$query = http_build_query(['query' => $data]);
			}
			$url .= '&' . $query;
			$response = $this->send_request($url);
		}

        if($element == 'widget_set'){
            if (!$response || empty($response['response']['widgets'])) {
                return FALSE;
            }
            return $response['response']['widgets'];
        } else {
            if (!$response || empty($response['response'][$element])) {
                return FALSE;
            }
            return $response['response'][$element];
        }
	}

	private function send_request(
		$url,
		$data = NULL,
		$json_encode = FALSE,
		$http_header = 'Content-Type: application/json'
	) {
		$this->_response = NULL;
		$curl = $this->get_curl();
		curl_setopt($curl, CURLOPT_URL, $url);

		if (!empty($data)) {
			if ($json_encode) {
				$post_fields = json_encode($data);
			} else {
				$post_fields = http_build_query($data);
			}
			curl_setopt($curl, CURLOPT_POSTFIELDS, $post_fields);
		}
		curl_setopt($curl, CURLOPT_HTTPHEADER, [$http_header]);
		
		curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt');
        curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt');

		$this->_response = json_decode(curl_exec($curl), TRUE);
		
		if (curl_errno($curl)) {
			$this->_response = 'cURL error: ' . curl_error($curl);
		}

		return $this->_response;
	}

    /**
     * Отправка запроса
     *
     * @param string $link
     * @param array $data
     * @param string $method
     * @return array
     * @internal param $post
     */
    public function sendCURL($link, $data, $method = 'POST', $date_start = false)
    {
        if ($method == 'GET') {
            if (strpos($link, '?') === false ) {
                $link .= '?';
            } else {
                $link .= '&';
            }
            if (!empty($data)) {
                foreach ($data as $k => &$v) {
                    if (is_array($v)) {
                        $tmp = $v;
                        $v = '';
                        foreach($tmp as $vv) {
                            $v = $k . '[]=' . $vv;
                        }
                    } else {
                        $v = $k . '=' . $v;
                    }
                }
                $link = $link . implode('&', $data);
            }
        }
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
		if($date_start){
			curl_setopt($curl, CURLOPT_HTTPHEADER,array('IF-MODIFIED-SINCE: '.$date_start.' 00:00:00'));
		}
        curl_setopt($curl, CURLOPT_URL, $link);
        if ($method == 'POST') {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($curl,CURLOPT_POSTFIELDS,json_encode($data));
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        }
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt');
        curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $result = curl_exec($curl);
        $out = json_decode($result);
		$code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        if (isset($out->response)) {
            return array('out' => $out->response, 'code' => $code);
        } else {
            return $out;
        }
    }
	
	public function sendCURLv2($link, $data, $method = 'POST', $date_start = false)
    {
        if ($method == 'GET') {
            if (strpos($link, '?') === false ) {
                $link .= '?';
            } else {
                $link .= '&';
            }
            if (!empty($data)) {
                foreach ($data as $k => &$v) {
                    if (is_array($v)) {
                        $tmp = $v;
                        $v = '';
                        foreach($tmp as $vv) {
                            $v = $k . '[]=' . $vv;
                        }
                    } else {
                        $v = $k . '=' . $v;
                    }
                }
                $link = $link . implode('&', $data);
            }
        }
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
		if($date_start){
			curl_setopt($curl, CURLOPT_HTTPHEADER,array('IF-MODIFIED-SINCE: '.$date_start.' 00:00:00'));
		}
        curl_setopt($curl, CURLOPT_URL, $link);
        if ($method == 'POST') {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($curl,CURLOPT_POSTFIELDS,json_encode($data));
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        }
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt');
        curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $result = curl_exec($curl);
        $out = json_decode($result);
		
		$code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        if (isset($out->_embedded) && !empty($out->_embedded->items)) {
            return array('out' =>$out->_embedded->items, 'code' => $code);
        } else {
            return $out;
        }
    }
	
	private function send_get_request($url) {
		$curl=curl_init();
		curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
		curl_setopt($curl,CURLOPT_URL,$url);
		curl_setopt($curl,CURLOPT_CUSTOMREQUEST,'GET');
		curl_setopt($curl,CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
		curl_setopt($curl,CURLOPT_HEADER,false);
		curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt'); 
		curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt'); 		
		curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
		curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
		 
		$out=curl_exec($curl);
		$code=curl_getinfo($curl,CURLINFO_HTTP_CODE);
		$response=json_decode($out,true);
		
		if($response['_embedded']){
			return array('out' => $response['_embedded'], 'code' => $code);
		} else {
            return null;
        }
	}
}